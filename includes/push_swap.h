/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 15:06:07 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/11 14:45:21 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H
# include "libft.h"

int		*getnumbers(int count, const char *argv[]);
int		checknbr(int nbr, const char *arg);
int		checkdup(int *stack);
int		is_sorted(int *stack);
void	errmsg(int *stack, int *stack2);

void	get_instr(int *stack, int *stack2);
int		apply_instr(char *instr, int *stack, int *stack2);
void	apply_instr_swap(char *instr, int *stack, int *stack2);
void	apply_instr_push(char *instr, int *stack, int *stack2);
void	apply_instr_rotate(char *instr, int *stack, int *stack2);
void	apply_instr_rrotate(char *instr, int *stack, int *stack2);

void	apply_sorting(int *stack, int *stack2, const char *argv);
void	apply_oper(char *oper, int *stack, int *stack2, char **result);
int		count_sorted(int *stack);

void	apply_s(int *stack, int *stack2, char **result, char *oper);
void	apply_rr(int *stack, int *stack2, char **result, char *oper);

void	sort_stack(int *stack, int *stack2, char **result);
int		*getsorted(int *stack, int trimflag);
int		*trimsorted(int *sorted, int *stack);
void	manage_unsorted(int *stack, int *stack2, char **result, int sorted);

int		find_elem_less(int *stack, int mid);
int		find_elem_more(int *stack, int mid);
int		find_elem(int *stack, int elem);

void	pull_elems(int *stack, int *stack2, int *sorted, char **result);
int		pull_check(int *stack, int *sorted);
int		is_short(int *stack, int *stack2, char **result);
int		getmid(int *sorted);

int		getflags(int argc, const char *argv[]);
void	count_lines(char *result);
int		flags_misuse(void);
void	showsorted(int *stack);
void	visualize(int *stack, char *operations);
void	clean_up(int **stack, int **stack2);

void	print_stack(int *stack, char *msg);
int		*get_copy(int *stack);

#endif
