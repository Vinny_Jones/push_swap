/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/03 14:10:48 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/20 14:02:57 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

void	apply_thousand_sep(char **result, int len)
{
	int		new_len;
	int		i;
	int		j;
	char	*temp;

	temp = *result;
	i = 0;
	while (temp[i] == '0' || temp[i] == '-')
		i++;
	new_len = len - i + (len - i - 1) / 3;
	new_len = (temp[0] == '-') ? new_len + 1 : new_len;
	new_len = MAX(len, new_len);
	*result = ft_strnew(new_len);
	j = 0;
	while (len > i)
	{
		(j == 3) ? (*result)[--new_len] = ',' : 0;
		j = (j == 3) ? 1 : j + 1;
		(*result)[--new_len] = temp[--len];
	}
	while (new_len)
		(*result)[--new_len] = '0';
	(*result)[0] = (temp[0] == '-') ? '-' : (*result)[0];
	free(temp);
}

void	apply_sign_flag(char *fmt, char **str)
{
	char *temp;

	if (CFMT('d') || CFMT('i') || ACFMT('f'))
	{
		temp = *str;
		*str = ft_strnew(ft_strlen(*str) + 1);
		(*str)[0] = (CFMT('+')) ? '+' : ' ';
		ft_strcpy(&(*str)[1], temp);
		free(temp);
	}
}

void	apply_hash_flag(char **result, char *fmt, int len)
{
	char	*temp;
	int		i;

	temp = *result;
	i = 0;
	while (temp[i] == '0')
		i++;
	if (ACFMT('p') || (i == 0 && ACFMT('o')) || (i < len && ACFMT('x')))
	{
		*result = ft_strnew(len + ((ACFMT('o')) ? 1 : 2));
		(*result)[0] = '0';
		if (CFMT('x') || CFMT('p'))
			(*result)[1] = 'x';
		else if (CFMT('X'))
			(*result)[1] = 'X';
		ft_strcat(*result, temp);
		free(temp);
	}
}

void	apply_nul_flag(char **result, char *fmt, int prec)
{
	char	sign;
	char	x;
	int		i;

	if (!CFMT('.') || prec < 0 || ACFMT('f'))
	{
		sign = 0;
		i = 0;
		while ((*result)[i] == ' ')
			i++;
		sign = ((*result)[i] == '-') ? '-' : sign;
		sign = (CFMT(' ') && !sign) ? ' ' : sign;
		sign = ((*result)[i] == '+') ? '+' : sign;
		i = 0;
		while (!ft_isalnum((*result)[i]))
			(*result)[i++] = '0';
		x = (ft_tolower((*result)[++i]) == 'x') ? (*result)[i] : 0;
		(*result)[i] = (x) ? '0' : (*result)[i];
		i = 0;
		if (sign && (CFMT('d') || CFMT('i') || ACFMT('f')))
			(*result)[i++] = sign;
		if (x)
			(*result)[++i] = x;
	}
}

int		isnulflag(char *fmt)
{
	while (*fmt)
	{
		if (*fmt == '0' && *(fmt - 1) != '.')
			return (1);
		if (ft_isdigit(*fmt))
			while (ft_isdigit(*fmt))
				fmt++;
		fmt++;
	}
	return (0);
}
