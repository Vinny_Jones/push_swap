/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_n.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/21 11:38:22 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/21 12:51:39 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

void	print_nspec(va_list ap_f, va_list ap, char *fmt, int count)
{
	int		*nb;
	va_list	temp;

	va_copy(temp, ap_f);
	if (fmt_isnumarg(fmt))
		nb = getnumptr(temp, ft_atoi(fmt + 1));
	else
		nb = va_arg(ap, int*);
	*nb = count;
	va_end(temp);
}

int		*getnumptr(va_list ap, int n)
{
	int	*result;

	while (--n > 0)
		va_arg(ap, int*);
	result = va_arg(ap, int*);
	return (result);
}
