/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_str.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/06 12:35:18 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/18 11:59:14 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int		print_str(va_list ap, char *fmt, int width, int prec)
{
	int	num_arg;

	num_arg = fmt_isnumarg(fmt);
	prec = (CFMT('.')) ? prec : -1;
	if (CFMT('l') || CFMT('S'))
		return (print_wstr(get_wstr(ap, num_arg, fmt), width, prec, fmt));
	else
		return (print_s(get_str(ap, num_arg, fmt), width, prec, fmt));
}

int		print_s(char *str, int width, int prec, char *fmt)
{
	int		len;
	int		j;
	char	filler;

	if (!str)
		str = "(null)";
	len = ft_strlen(str);
	len = (prec < 0) ? len : MIN(len, prec);
	filler = isnulflag(fmt) ? '0' : ' ';
	j = 0;
	while (!CFMT('-') && j++ < width - len)
		write(1, &filler, 1);
	write(1, str, len);
	j = 0;
	while ((CFMT('-') || width < 0) && j++ < ABS(width) - len)
		write(1, " ", 1);
	return (MAX(ABS(width), len));
}

char	*get_str(va_list ap, int num_arg, char *fmt)
{
	char	*res;
	int		n_arg;

	if (num_arg)
	{
		n_arg = ft_atoi(fmt + 1);
		while (--n_arg)
			va_arg(ap, char*);
		res = va_arg(ap, char*);
	}
	else
		res = va_arg(ap, char*);
	return (res);
}
