/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/24 17:48:27 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/31 16:36:57 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include "libft.h"

static int	is_str(char *fd_map)
{
	char	*temp;

	temp = fd_map;
	while (fd_map && *fd_map)
	{
		if (*fd_map == '\n')
			return (fd_map - temp);
		else
			fd_map++;
	}
	return (-1);
}

static char	*parse_fd(char **fd_map)
{
	int		len;
	char	*line;
	char	*temp;

	if ((len = is_str(*fd_map)) >= 0)
	{
		line = ft_strndup(*fd_map, len);
		temp = *fd_map;
		*fd_map = ft_strdup(&(*fd_map)[len + 1]);
		free(temp);
	}
	else
	{
		line = ft_strdup(*fd_map);
		free(*fd_map);
		*fd_map = NULL;
	}
	return (line);
}

static void	save_fd(char **fd_map, char *buff)
{
	char *temp;

	temp = *fd_map;
	if (*fd_map == NULL)
		*fd_map = ft_strdup(buff);
	else
		*fd_map = ft_strjoin(*fd_map, buff);
	free(temp);
}

int			get_next_line(const int fd, char **line)
{
	static char	*fd_map[MAX_FD];
	char		buff[BUFF_SIZE + 1];
	int			read_ch;

	if (fd < 0 || fd > MAX_FD || BUFF_SIZE < 1)
		return (-1);
	read_ch = BUFF_SIZE;
	while (is_str(fd_map[fd]) < 0 && read_ch)
	{
		if ((read_ch = read(fd, buff, BUFF_SIZE)) < 0)
			return (-1);
		buff[read_ch] = '\0';
		save_fd(&fd_map[fd], buff);
	}
	if (*fd_map[fd])
	{
		if (!(*line = parse_fd(&fd_map[fd])))
			return (-1);
		else
			return (1);
	}
	if (*line == NULL)
		*line = ft_strdup("");
	return (0);
}
