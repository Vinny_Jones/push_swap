/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 19:43:58 by apyvovar          #+#    #+#             */
/*   Updated: 2016/11/23 19:43:59 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char	*ft_strchr(const char *str, int c)
{
	while (*str)
		if (*str == (char)c)
			return ((char *)str);
		else
			str++;
	if (c == '\0')
		return ((char *)str);
	return (NULL);
}
