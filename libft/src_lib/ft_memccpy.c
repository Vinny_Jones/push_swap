/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 12:21:41 by apyvovar          #+#    #+#             */
/*   Updated: 2016/11/23 12:21:42 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	int				j;
	unsigned char	tmp;

	j = 0;
	while (n-- > 0)
	{
		tmp = ((unsigned char *)src)[j];
		((unsigned char *)dst)[j] = ((unsigned char *)src)[j];
		j++;
		if (tmp == (unsigned char)c)
			return (&((unsigned char *)dst)[j]);
	}
	return (NULL);
}
