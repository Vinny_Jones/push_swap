/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 14:52:29 by apyvovar          #+#    #+#             */
/*   Updated: 2016/11/23 14:52:40 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *dest, const char *src, size_t n)
{
	char	*dst;

	dst = dest;
	while (*dst)
		dst++;
	while (n-- > 0 && *src)
		*dst++ = *src++;
	*dst = '\0';
	return (dest);
}
