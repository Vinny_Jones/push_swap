/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 18:59:05 by apyvovar          #+#    #+#             */
/*   Updated: 2016/11/26 19:36:08 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr(int n)
{
	if (n < 0)
		ft_putchar('-');
	if (ABS(n) >= 0 && ABS(n) <= 9)
		ft_putchar(ABS(n) + '0');
	else
	{
		ft_putnbr(ABS(n / 10));
		ft_putchar(ABS(n % 10) + '0');
	}
}
