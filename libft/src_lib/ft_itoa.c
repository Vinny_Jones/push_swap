/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 16:35:43 by apyvovar          #+#    #+#             */
/*   Updated: 2016/12/01 16:14:54 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	char	*result;
	int		size;
	int		i;

	i = n;
	size = (n < 0) ? 2 : 1;
	while (i /= 10)
		size++;
	result = (char *)malloc(size + 1);
	if (!result)
		return (NULL);
	if (n < 0)
		result[0] = '-';
	result[size] = '\0';
	while (size--)
	{
		result[size] = ABS(n % 10) + '0';
		n /= 10;
		if (n == 0)
			break ;
	}
	return (result);
}
