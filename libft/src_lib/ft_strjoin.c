/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 16:35:43 by apyvovar          #+#    #+#             */
/*   Updated: 2016/12/14 14:32:21 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*result;
	int		len1;

	if (!s1 || !s2)
		return (NULL);
	len1 = ft_strlen(s1);
	result = ft_strnew(len1 + ft_strlen(s2));
	if (!result)
		return (NULL);
	result = ft_strcat(result, s1);
	ft_strcat(result + len1, s2);
	return (result);
}
