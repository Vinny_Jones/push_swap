/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 20:03:14 by apyvovar          #+#    #+#             */
/*   Updated: 2016/11/23 20:03:18 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *str, int c)
{
	int i;

	i = ft_strlen(str);
	if (c == '\0')
		return ((char *)(str + i));
	while (i-- > 0)
		if (*(str + i) == (char)c)
			return ((char *)(str + i));
	return (NULL);
}
