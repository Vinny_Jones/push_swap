/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 13:16:44 by apyvovar          #+#    #+#             */
/*   Updated: 2016/11/22 13:16:45 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	int				j;

	j = 0;
	while (n-- > 0)
	{
		((unsigned char *)dst)[j] = ((unsigned char *)src)[j];
		j++;
	}
	return (dst);
}
