/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 12:43:01 by apyvovar          #+#    #+#             */
/*   Updated: 2016/11/29 13:35:32 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *s1, size_t n)
{
	char	*res;
	int		i;
	size_t	size;

	size = MIN(n, ft_strlen(s1)) + 1;
	res = (char *)malloc(sizeof(char) * size);
	if (!res)
		return (NULL);
	i = -1;
	while (s1[++i] && --size > 0)
		res[i] = s1[i];
	res[i] = '\0';
	return (res);
}
