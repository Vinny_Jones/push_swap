/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 14:15:01 by apyvovar          #+#    #+#             */
/*   Updated: 2016/11/23 14:15:02 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char	*ft_strncpy(char *dest, const char *src, size_t len)
{
	char	*res;

	res = dest;
	while (*src && len > 0)
	{
		*res++ = *src++;
		len--;
	}
	while (len > 0)
	{
		*res++ = 0;
		len--;
	}
	return (dest);
}
