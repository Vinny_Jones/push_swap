# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/01/25 15:09:15 by apyvovar          #+#    #+#              #
#    Updated: 2017/02/11 14:24:32 by apyvovar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

FLAGS = -Wall -Wextra -Werror
CC = gcc $(FLAGS)
SRC_DIR = src/
LIB_DIR = libft/
INC_DIR = includes
NAME = makeprograms
CHECKER = checker
PUSH_SWAP = push_swap
SRC_FILES_CHECKER = checker.c parse.c instrutions.c common_tools.c
SRC_FILES_PUSH_SWAP = push_swap.c parse.c sort.c instrutions.c flags_mgmt.c \
	 tools.c tools2.c stack_mgmt.c common_tools.c
SRC_CH = $(addprefix $(SRC_DIR), $(SRC_FILES_CHECKER))
SRC_PS = $(addprefix $(SRC_DIR), $(SRC_FILES_PUSH_SWAP))

all: $(NAME)

.PHONY: $(NAME)
$(NAME): $(CHECKER) $(PUSH_SWAP)

$(CHECKER):
	make -C $(LIB_DIR)
	$(CC) $(SRC_CH) -I $(INC_DIR) -I $(LIB_DIR)$(INC_DIR) -L $(LIB_DIR) -lft \
	-o $(CHECKER)

$(PUSH_SWAP):
	make -C $(LIB_DIR)
	$(CC) $(SRC_PS) -I $(INC_DIR) -I $(LIB_DIR)$(INC_DIR) -L $(LIB_DIR) -lft \
	-o $(PUSH_SWAP)

clean:
	make -C $(LIB_DIR) clean

fclean: clean
	/bin/rm -f $(LIB_DIR)libft.a
	/bin/rm -f $(CHECKER) $(PUSH_SWAP)

re: fclean all
