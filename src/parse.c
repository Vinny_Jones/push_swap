/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 19:29:00 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/07 19:51:39 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		*getnumbers(int count, const char *argv[])
{
	int	*stack;
	int	i;
	int	error;

	error = 0;
	if (!(stack = (int *)malloc(sizeof(int) * count)))
		return (NULL);
	i = 0;
	stack[i++] = count - 1;
	while (i < count)
	{
		stack[count - i] = ft_atoi(argv[i]);
		if ((error = checknbr(stack[count - i], argv[i])))
			break ;
		i++;
	}
	if (error || checkdup(stack))
	{
		free(stack);
		return (NULL);
	}
	return (stack);
}

int		checknbr(int nbr, const char *arg)
{
	int	len;

	while (ft_isspace(*arg))
		arg++;
	if (*arg == '-' && nbr >= 0)
		return (1);
	if (*arg == '-' || *arg == '+')
		arg++;
	len = ft_strlen(arg) - 1;
	if (nbr == 0 && arg[len] == '0')
		len--;
	else
		while (nbr)
		{
			if (ABS(nbr % 10) + '0' != arg[len])
				return (1);
			len--;
			nbr /= 10;
		}
	if (len != -1)
		return (1);
	return (0);
}

int		checkdup(int *stack)
{
	int	count;
	int i;
	int j;

	count = stack[0];
	i = 1;
	while (i < count)
	{
		j = i + 1;
		while (j <= count)
			if (stack[i] == stack[j++])
				return (1);
		i++;
	}
	return (0);
}

int		is_sorted(int *stack)
{
	int	i;

	i = 0;
	while (++i < stack[0])
		if (stack[i] < stack[i + 1])
			return (0);
	return (1);
}
