/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/02 12:07:11 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/11 12:46:19 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		pull_check(int *stack, int *sorted)
{
	int i;
	int	j;

	i = 0;
	while (++i <= sorted[0])
	{
		j = 0;
		while (++j <= stack[0])
			if (stack[j] == sorted[i])
				break ;
			else if (j == stack[0])
				return (0);
	}
	return (1);
}

int		is_short(int *stack, int *stack2, char **result)
{
	int i;

	if (is_sorted(stack))
		return (0);
	i = 0;
	while (++i < stack[0] - 1 && stack[i] > stack[i + 1])
		if (i == stack[0] - 2 && stack[stack[0]] < stack[stack[0] - 2])
		{
			apply_s(stack, stack2, result, "sa");
			return (1);
		}
		else if (i == stack[0] - 2 && stack[stack[0]] > stack[1])
		{
			apply_oper("ra", stack, stack2, result);
			return (1);
		}
	i = 1;
	while (++i < stack[0] && stack[i] > stack[i + 1])
		if (i == stack[0] - 1 && stack[1] < stack[stack[0]])
		{
			apply_oper("rra", stack, stack2, result);
			return (1);
		}
	return (0);
}

int		getmid(int *sorted)
{
	int	index;

	index = sorted[0] / 2 + 1;
	if (sorted[0] > 3)
		index = (index % 2) ? index : index + 1;
	else
		index = sorted[0];
	return (sorted[index]);
}
