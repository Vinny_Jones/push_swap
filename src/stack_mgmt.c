/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_mgmt.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/11 14:21:27 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/11 14:41:14 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		*getsorted(int *stack, int trimflag)
{
	int	*result;
	int	j;
	int	i;
	int	temp;

	result = (int *)malloc(sizeof(int) * (stack[0] + 1));
	result[0] = stack[0];
	i = 0;
	while (++i <= stack[0])
		result[i] = stack[i];
	i = 0;
	while (++i < stack[0])
	{
		j = i;
		while (++j <= stack[0])
			if (result[i] < result[j])
			{
				temp = result[i];
				result[i] = result[j];
				result[j] = temp;
			}
	}
	if (trimflag)
		return (trimsorted(result, stack));
	return (result);
}

int		*trimsorted(int *sorted, int *stack)
{
	int		*new;
	int		j;
	int		i;

	i = 0;
	while (stack[++i] != sorted[1])
		;
	j = 1;
	while (j <= stack[0] && stack[i++] == sorted[j++])
		if (i == stack[0] + 1)
			i = 1;
	j--;
	new = (int *)malloc(sizeof(int) * (sorted[0] - j + 2));
	new[0] = sorted[0] - j + 1;
	i = 1;
	while (j <= sorted[0])
		new[i++] = sorted[j++];
	free(sorted);
	return (new);
}

int		*get_copy(int *stack)
{
	int	i;
	int	*result;

	result = (int *)malloc(sizeof(int) * (stack[0] + 1));
	if (!result)
		return (NULL);
	i = 0;
	while (i <= stack[0])
	{
		result[i] = stack[i];
		i++;
	}
	return (result);
}

void	print_stack(int *stack, char *msg)
{
	int	i;

	i = 0;
	ft_printf("%s[%i]:\n", msg, stack[0]);
	if (stack[0] == 0)
		ft_printf("[empty]");
	while (++i <= stack[0])
		ft_printf("[%i] = %i ", i, stack[i]);
	ft_printf("\n");
}
