/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/11 12:46:07 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/11 14:23:30 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		find_elem_less(int *stack, int mid)
{
	int	i;

	i = stack[0] + 1;
	while (--i > 0)
		if (stack[i] <= mid)
			return (i);
	return (0);
}

int		find_elem_more(int *stack, int mid)
{
	int	i;

	i = stack[0] + 1;
	while (--i > 0)
		if (stack[i] >= mid)
			return (i);
	return (0);
}

int		find_elem(int *stack, int elem)
{
	int	i;

	i = 0;
	while (++i <= stack[0])
		if (stack[i] == elem)
			return (1);
	return (0);
}
