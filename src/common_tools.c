/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common_tools.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/07 18:56:39 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/11 00:03:36 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	clean_up(int **stack, int **stack2)
{
	if (stack && *stack)
	{
		free(*stack);
		*stack = NULL;
	}
	if (stack2 && *stack2)
	{
		free(*stack2);
		*stack2 = NULL;
	}
}

void	errmsg(int *stack, int *stack2)
{
	clean_up(&stack, &stack2);
	ft_putstr_fd("Error\n", 2);
}
