/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/27 14:06:38 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/11 12:46:03 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	apply_oper(char *oper, int *stack, int *stack2, char **result)
{
	int		len;
	char	*temp;

	apply_instr(oper, stack, stack2);
	len = ft_strlen(*result);
	temp = *result;
	*result = (char *)malloc(sizeof(char) * (len + ft_strlen(oper) + 2));
	**result = '\0';
	if (temp)
		ft_strcpy(*result, temp);
	ft_strcat(*result, oper);
	ft_strcat(*result, "\n");
	if (temp)
		free(temp);
}

void	apply_s(int *stack, int *stack2, char **result, char *oper)
{
	if (stack[stack[0]] > stack[stack[0] - 1] && stack[0] > 1 && \
		stack2[stack2[0]] < stack2[stack2[0] - 1] && stack2[0] > 1)
		apply_oper("ss", stack, stack2, result);
	else
		apply_oper(oper, stack, stack2, result);
}

void	apply_rr(int *stack, int *stack2, char **result, char *oper)
{
	if (stack[1] < stack[stack[0]] && stack[0] > 1 && \
		stack2[1] > stack2[stack2[0]] && stack2[0] > 1)
		apply_oper("rrr", stack, stack2, result);
	else
		apply_oper(oper, stack, stack2, result);
}

int		count_sorted(int *stack)
{
	int	i;
	int	res;

	res = 0;
	i = 0;
	while (++i < stack[0] - 1)
		if (stack[i] > stack[i + 1])
			res++;
	return (res);
}
