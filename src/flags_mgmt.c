/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags_mgmt.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/07 16:57:00 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/11 14:45:40 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		getflags(int argc, const char *argv[])
{
	int	i;

	if (*argv[1] == '-' && !ft_isdigit(argv[1][1]))
	{
		if (argc == 2)
		{
			errmsg(NULL, NULL);
			return (0);
		}
		i = 1;
		while (argv[1][i])
			if (argv[1][i] == 'l' || argv[1][i] == 'v' || argv[1][i] == 'h' || \
				argv[1][i] == 's')
				i++;
			else
				return (flags_misuse());
		return (1);
	}
	return (2);
}

void	count_lines(char *result)
{
	int	count;

	count = 0;
	while (*result)
	{
		if (*result == '\n')
			count++;
		result++;
	}
	ft_printf("Total amount of operations used - %i\n", count);
}

void	visualize(int *stack, char *operations)
{
	int		*stack2;
	char	oper[4];
	int		i;
	char	*temp;

	stack2 = (int *)malloc(sizeof(int) * (stack[0] + 1));
	stack2[0] = 0;
	print_stack(stack, "Starting stack");
	if (stack2)
		while (*operations)
		{
			i = 0;
			while (i < 4)
				oper[i++] = '\0';
			temp = ft_strchr(operations, '\n');
			ft_strncpy(oper, operations, temp - operations);
			operations = temp + 1;
			apply_instr(oper, stack, stack2);
			ft_printf("=========================\n");
			ft_printf("Operation - [%s]\n", oper);
			print_stack(stack, "Stack A");
			print_stack(stack2, "Stack B");
		}
	clean_up(&stack2, NULL);
}

void	showsorted(int *stack)
{
	int	*sorted;

	sorted = getsorted(stack, 0);
	print_stack(sorted, "Sorted stack");
	clean_up(&sorted, NULL);
}

int		flags_misuse(void)
{
	ft_putstr("Error: wrong flag used\n\n");
	ft_putstr("Usage:\t./push_swap [flags] [set of ints]\n");
	ft_putstr("Flags:\n\t-v  -  visualize operations;\n");
	ft_putstr("\t-l  -  show amount of used operations;\n");
	ft_putstr("\t-s  -  show sorted stack first;\n");
	ft_putstr("\t-h  -  hide operations.\n");
	return (0);
}
