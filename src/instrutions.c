/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instrutions.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/25 21:17:16 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/31 16:53:54 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		apply_instr(char *instr, int *stack, int *stack2)
{
	if (CMP(instr, "sa") || CMP(instr, "sb") || CMP(instr, "ss"))
		apply_instr_swap(instr, stack, stack2);
	else if (CMP(instr, "pa") || CMP(instr, "pb"))
		apply_instr_push(instr, stack, stack2);
	else if (CMP(instr, "ra") || CMP(instr, "rb") || CMP(instr, "rr"))
		apply_instr_rotate(instr, stack, stack2);
	else if (CMP(instr, "rra") || CMP(instr, "rrb") || CMP(instr, "rrr"))
		apply_instr_rrotate(instr, stack, stack2);
	else
		return (0);
	return (1);
}

void	apply_instr_rrotate(char *instr, int *stack, int *stack2)
{
	int	temp;
	int	i;

	if (stack[0] > 1 && (instr[2] == 'a' || instr[2] == 'r'))
	{
		temp = stack[1];
		i = 0;
		while (++i < stack[0])
			stack[i] = stack[i + 1];
		stack[i] = temp;
	}
	if (stack2[0] > 1 && (instr[2] == 'b' || instr[2] == 'r'))
	{
		temp = stack2[1];
		i = 0;
		while (++i < stack2[0])
			stack2[i] = stack2[i + 1];
		stack2[i] = temp;
	}
}

void	apply_instr_rotate(char *instr, int *stack, int *stack2)
{
	int	temp;
	int count;

	count = stack[0];
	if (count > 1 && (instr[1] == 'a' || instr[1] == 'r'))
	{
		temp = stack[count];
		while (--count >= 1)
			stack[count + 1] = stack[count];
		stack[1] = temp;
	}
	count = stack2[0];
	if (count > 1 && (instr[1] == 'b' || instr[1] == 'r'))
	{
		temp = stack2[count];
		while (--count >= 1)
			stack2[count + 1] = stack2[count];
		stack2[1] = temp;
	}
}

void	apply_instr_push(char *instr, int *stack, int *stack2)
{
	if (instr[1] == 'a' && stack2[0] > 0)
	{
		stack[stack[0] + 1] = stack2[stack2[0]];
		stack[0]++;
		stack2[0]--;
	}
	else if (instr[1] == 'b' && stack[0] > 0)
	{
		stack2[stack2[0] + 1] = stack[stack[0]];
		stack[0]--;
		stack2[0]++;
	}
}

void	apply_instr_swap(char *instr, int *stack, int *stack2)
{
	int temp;
	int count;

	count = stack[0];
	if (count > 1 && (instr[1] == 'a' || instr[1] == 's'))
	{
		temp = stack[count];
		stack[count] = stack[count - 1];
		stack[count - 1] = temp;
	}
	count = stack2[0];
	if (count > 1 && (instr[1] == 'b' || instr[1] == 's'))
	{
		temp = stack2[count];
		stack2[count] = stack2[count - 1];
		stack2[count - 1] = temp;
	}
}
