/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/31 16:33:39 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/11 11:51:27 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		main(int argc, const char *argv[])
{
	int	*stack;
	int	*stack2;

	if (argc > 1)
	{
		stack = getnumbers(argc, argv);
		stack2 = (int *)malloc(sizeof(int) * argc);
		if (!stack || !stack2)
			errmsg(stack, stack2);
		else
		{
			stack2[0] = 0;
			get_instr(stack, stack2);
			clean_up(&stack, &stack2);
		}
	}
	return (0);
}

void	get_instr(int *stack, int *stack2)
{
	char	*instr;
	int		readline;
	int		error;

	error = 0;
	instr = NULL;
	while ((readline = get_next_line(0, &instr)) > 0)
	{
		if (error || !apply_instr(instr, stack, stack2))
			error = 1;
		if (instr)
			free(instr);
		instr = NULL;
		if (readline == -1)
			break ;
	}
	if (instr)
		free(instr);
	if (readline == -1 || error)
		ft_putstr("Error\n");
	else if (is_sorted(stack) && stack2[0] == 0)
		ft_putstr("OK\n");
	else
		ft_putstr("KO\n");
}
