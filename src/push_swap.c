/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/07 23:37:32 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/11 14:41:33 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		main(int argc, const char *argv[])
{
	int	*stack;
	int	*stack2;
	int	flags;

	if (argc > 1 && (flags = getflags(argc, argv)))
	{
		if (flags == 1)
			stack = getnumbers(argc - 1, argv + 1);
		else
			stack = getnumbers(argc, argv);
		stack2 = (int *)malloc(sizeof(int) * argc);
		if (!stack || !stack2)
			errmsg(stack, stack2);
		else
		{
			stack2[0] = 0;
			if (!is_sorted(stack))
				apply_sorting(stack, stack2, argv[1]);
			clean_up(&stack, &stack2);
		}
	}
	return (0);
}

void	apply_sorting(int *stack, int *stack2, const char *argv)
{
	char	*result;
	int		*stack_copy;

	result = NULL;
	stack_copy = get_copy(stack);
	sort_stack(stack, stack2, &result);
	if (ft_strchr(argv, 's'))
		showsorted(stack_copy);
	if (ft_strchr(argv, 'l') && result)
		count_lines(result);
	if (ft_strchr(argv, 'v') && result)
		visualize(stack_copy, result);
	if (!ft_strchr(argv, 'h') && result)
		ft_putstr(result);
	clean_up(&stack_copy, NULL);
	if (result)
		free(result);
}

void	sort_stack(int *stack, int *stack2, char **result)
{
	int		*sorted;
	int		mid;
	int		index;

	sorted = getsorted(stack, 1);
	mid = getmid(sorted);
	while ((index = find_elem_less(stack, mid)) && sorted[0] > 2)
	{
		if (is_short(stack, stack2, result))
			break ;
		if (stack[stack[0]] <= mid)
			apply_oper("pb", stack, stack2, result);
		else if (index < stack[0] / 2 + 1)
			apply_rr(stack, stack2, result, "rra");
		else
			apply_oper("ra", stack, stack2, result);
	}
	manage_unsorted(stack, stack2, result, sorted[0]);
	while (stack2[0] > 0 && !pull_check(stack, sorted))
	{
		pull_elems(stack, stack2, sorted, result);
		if (!is_sorted(stack))
			sort_stack(stack, stack2, result);
	}
	free(sorted);
}

void	manage_unsorted(int *stack, int *stack2, char **result, int sorted)
{
	if (!is_sorted(stack))
	{
		if (sorted > 2)
			sort_stack(stack, stack2, result);
		else if (sorted > 1)
		{
			while (stack[1] < stack[stack[0]] || stack[1] < stack[2])
				apply_rr(stack, stack2, result, "rra");
			if (stack[stack[0]] > stack[stack[0] - 1])
				apply_s(stack, stack2, result, "sa");
		}
		else
		{
			if (stack[stack[0]] > stack[stack[0] - 1])
				apply_oper("ra", stack, stack2, result);
			else
				while (stack[1] < stack[stack[0]])
					apply_rr(stack, stack2, result, "rra");
		}
	}
}

void	pull_elems(int *stack, int *stack2, int *sorted, char **result)
{
	int	count;
	int	i;
	int index;
	int	mid;

	i = 0;
	while (++i < sorted[0] && find_elem(stack, sorted[i]))
		;
	count = (sorted[0] - i) / 2 + 1;
	if (count <= 4)
		count = (i == sorted[0]) ? 1 : 2;
	else
		count = (count % 2) ? count - 1 : count;
	mid = sorted[i + count - 1];
	while ((index = find_elem_more(stack2, mid)))
	{
		if (stack2[stack2[0]] >= mid)
			apply_oper("pa", stack, stack2, result);
		else if (index == stack2[0] - 1)
			apply_s(stack, stack2, result, "sb");
		else if (index < stack2[0] / 2 + 1)
			apply_rr(stack, stack2, result, "rrb");
		else
			apply_oper("rb", stack, stack2, result);
	}
}
