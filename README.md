PUSH-SWAP

This project aimed to create an algorithm to sort data on a "stack", with a limited set of instructions, using
the lowest possible number of actions.

The game is composed of 2 stacks named A and B. Input in stack A.
Actions:

```
#!

sa : swap a - swap the first 2 elements at the top of stack a.
sb : swap b - swap the first 2 elements at the top of stack b.
ss : sa and sb at the same time.
pa : push a - take the first element at the top of b and put it at the top of a.
pb : push b - take the first element at the top of a and put it at the top of b.
ra : rotate a - shift up all elements of stack a by 1. The first element becomes the last one.
rb : rotate b - shift up all elements of stack b by 1. The first element becomes the last one.
rr : ra and rb at the same time.
rra : reverse rotate a - shift down all elements of stack a by 1. The last element becomes the first one.
```


Example:

```
#!

$>./push_swap 2 1 3 6 5 8
sa
pb
pb
pb
sa
pa
pa
pa
$>./push_swap 0 one 2 3
Error
$>
```